%global _empty_manifest_terminate_build 0
Name:		python-freezegun
Version:	1.5.1
Release:	1
Summary:	Let your Python tests travel through time
License:	Apache-2.0
URL:            https://pypi.io/project/freezegun
Source0:        %{pypi_source freezegun}
BuildArch:	noarch
%description
Simulate the datetime module with the python-freezegun library, allowing your python
tests to travel through time.


%package -n python3-freezegun
Summary:	Let your Python tests travel through time
Provides:	python-freezegun
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-pbr
BuildRequires:  python3-pytest
BuildRequires:  python3-dateutil
Requires:	python3-dateutil
%description -n python3-freezegun
Simulate the datetime module with the python-freezegun library, allowing your python
tests to travel through time.


%package help
Summary:	Development documents and examples for freezegun
Provides:	python3-freezegun-doc
%description help
Simulate the datetime module with the python-freezegun library, allowing your python
tests to travel through time.


%prep
%autosetup -n freezegun-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%check
%{__python3} setup.py test


%files -n python3-freezegun -f filelist.lst
%dir %{python3_sitelib}/*

%changelog
* Fri Nov 22 2024 Ge Wang <wang__ge@126.com> - 1.5.1-1
- Update to 1.5.1

* Tue May 30 2023 chenchen <chen_aka_jan@163.com> - 1.2.2-1
- Upgrade to 1.2.2

* Thu Jul 07 2022 xu_ping <xuping33@h-partners.com> - 1.2.1-1
- Update to 1.2.1

* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org> - 1.1.0-1
- Update to 1.1.0

* Wed Aug 05 2020 lingsheng <lingsheng@huawei.com> - 0.3.8-13
- Remove python2-freezegun subpackage

* Tue Nov 26 2019 Ling Yang <lingyang2@huawei.com> - 0.3.8-12
- Package init
